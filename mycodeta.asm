name "INFORMATIKA" ; memberikan nama program
org 100h ; menyediakan 100 byte kosong pada saat program dijalankan,
 ;untuk mengontrol jalannya program tersebut

 mov ax, 3 ; memasukan 3 ke register ax
 int 10h ; mencetak karakter/perintah di atasnya
 mov ax, 1003h ; memasukan 1003h ke register ax
 mov bx, 0 ; memasukan 0 ke register bx
 int 10h ; mencetak mencetak karakter/perintah di atasnya
mov ax, 0b800h ; memasukan 0b800h ke register ax
mov ds, ax ; memperbaharui ax ke register ds
mov [02h], 'J' ; memasukkan karakter 2 ke 02h
 ; atau mencetak angka 2
mov [04h], 'A' ; dan seterusnya
mov [06h], 'D'
mov [08h], 'W'
mov [0ah], 'A' 
mov [0ch], 'L'
mov [0eh], ' '
mov [10h], 'K'
mov [12h], 'U'
mov [14h], 'L'
mov [16h], 'I'
mov [18h], 'A'
mov [1ah], 'H'
mov [1ch], ' '
mov [1eh], 'T'
mov [20h], 'E'
mov [22h], 'K'
mov [24h], 'N'
mov [26h], 'I'
mov [28h], 'K'
mov [2ah], ' '
mov [2ch], 'I'
mov [2eh], 'N'
mov [30h], 'F'
mov [32h], 'O'
mov [34h], 'R'
mov [36h], 'M'
mov [38h], 'A'
mov [3ah], 'T'
mov [3ch], 'I'
mov [3eh], 'K'
mov [40h], 'A'
mov [42h], ' '
mov [44h], 'F'
mov [46h], 'A'
mov [48h], 'K'
mov [4ah], 'U'
mov [4ch], 'L'
mov [4eh], 'T' 
mov [50h], 'A'
mov [52h], 'S'
mov [54h], ' '
mov [56h], 'T'
mov [58h], 'E'
mov [5ah], 'K'
mov [5ch], 'N'
mov [5eh], 'O'
mov [60h], 'L'
mov [62h], 'O'
mov [64h], 'G'
mov [66h], 'I'
mov [68h], ' '
mov [6ah], 'I'
mov [6ch], 'N'
mov [6eh], 'D'
mov [70h], 'U'
mov [72h], 'S'
mov [74h], 'T'
mov [76h], 'R'
mov [78h], 'I'
mov [7ah], ' '
mov [7ch], 'U'
mov [7eh], 'A'
mov [80h], 'D'

mov cx, 64 ; mencetak karakter sebanyak 66 kali
mov di, 03h ; memasukan 03h ke register di
c: mov [di], 10001010b ; mencetak warna 1000 abu-abu gelap, 1010 hijau
 add di, 2 ; menambahkan nilai 2 ke register di
 loop c ; lompat/kembali ke label c 
 mov ah, 0 ; memasukan 0 ke AH
 int 16h ; int 16h adalah instruksi untuk memasukkan inputan dari keyboard
Ret ;untuk kembali dari suatu subrutin program,
 ;ke alamat terakhir subrutin tersebut di panggil.